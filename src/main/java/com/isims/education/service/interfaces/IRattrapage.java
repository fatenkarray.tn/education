package com.isims.education.service.interfaces;

import com.isims.education.persistance.entities.Rattrapage;

import java.time.LocalDateTime;
import java.util.List;

public interface IRattrapage {

		Rattrapage saveRattrapage(Rattrapage rattrapage);
	     Rattrapage updateRattrapage(Rattrapage rattrapage);
	     boolean deleteRattrapage(Long id);
	     List<Rattrapage> getListRattrapage();
	     Rattrapage getRattrapage(Long id);
	     Rattrapage findRattrapageByDate(LocalDateTime date);
	     int getQuantityOfRattrapage();
	     Rattrapage getRattrapageByIdRattrapage(Long id);
	
	
}
