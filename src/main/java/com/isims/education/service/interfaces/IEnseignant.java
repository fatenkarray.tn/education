package com.isims.education.service.interfaces;

import java.util.List;

import com.isims.education.persistance.entities.Enseignant;

public interface IEnseignant {

	Enseignant saveEnseignant(Enseignant enseignant);
	Enseignant updateEnseignant(Enseignant enseignant);
	     boolean deleteEnseignant(Long id);
	     List<Enseignant> getListEnseignant();
	     Enseignant getEnseignant(Long id);
	     Enseignant findEnseignantByName(String name);
	     int getQuantityOfEnseignant();
	     Enseignant getEnseignantByIdEnseignant(Long id);
	
	
}
