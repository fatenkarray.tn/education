package com.isims.education.service.interfaces;

import com.isims.education.persistance.entities.Conge;

import java.time.LocalDateTime;
import java.util.List;

public interface IConge {

		Conge saveConge(Conge conge);
	     Conge updateConge(Conge conge);
	     boolean deleteConge(Long id);
	     List<Conge> getListConge();
	     Conge getConge(Long id);

	     int getQuantityOfConge();
	     Conge getCongeByIdConge(Long id);
	
	
}
