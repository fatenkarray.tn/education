package com.isims.education.service.interfaces;


import com.isims.education.persistance.entities.Evaluation;

import java.util.List;

public interface IEvaluation {

		Evaluation saveEvaluation(Evaluation evaluation);
		Evaluation updateEvaluation(Evaluation evaluation);
	     boolean deleteEvaluation(Long id);
	     List<Evaluation> getListEvaluation();
	     Evaluation getEvaluation(Long id);
	     int getQuantityOfEvaluation();
	     Evaluation getEvaluationByIdEvaluation(Long id);
	
	
}
