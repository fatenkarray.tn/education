package com.isims.education.service.interfaces;

import com.isims.education.persistance.entities.PresenceEns;

import java.util.List;

public interface IPresenceEns {

		PresenceEns savePresenceEns(PresenceEns presenceEns);
	     PresenceEns updatePresenceEns(PresenceEns presenceEns);
	     boolean deletePresenceEns(Long id);
	     List<PresenceEns> getListPresenceEns();
	     PresenceEns getPresenceEns(Long id);
	     PresenceEns findPresenceEnsByDate(String date);
	     PresenceEns getPresenceEnsByIdPresenceEns(Long id);
	
	
}
