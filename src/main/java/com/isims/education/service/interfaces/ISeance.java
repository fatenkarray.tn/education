package com.isims.education.service.interfaces;


import com.isims.education.persistance.entities.Seance;

import java.util.List;

public interface ISeance {

		Seance saveSeance(Seance seance);
		Seance updateSeance(Seance seance);
	     boolean deleteSeance(Long id);
	     List<Seance> getListSeance();
	     Seance getSeance(Long id);
	     int getQuantityOfSeance();
	     Seance getSeanceByIdSeance(Long id);
	
	
}
