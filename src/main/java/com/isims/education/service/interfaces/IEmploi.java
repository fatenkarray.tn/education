package com.isims.education.service.interfaces;

import com.isims.education.persistance.entities.Emploi;

import java.util.List;

public interface IEmploi {

		Emploi saveEmploi(Emploi emploi);
	     Emploi updateEmploi(Emploi emploi);
	     boolean deleteEmploi(Long id);
	     List<Emploi> getListEmploi();
	     Emploi getEmploi(Long id);
	     int getQuantityOfEmploi();
	     Emploi getEmploiByIdEmploi(Long id);
	
	
}
