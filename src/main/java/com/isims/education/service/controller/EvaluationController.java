package com.isims.education.service.controller;

import com.isims.education.persistance.entities.Evaluation;
import com.isims.education.service.interfaces.IEvaluation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/evaluation")
public class EvaluationController {

	@Autowired
	IEvaluation evaluationservice;
	
	@RequestMapping(method = RequestMethod.POST, consumes="application/json", produces = "application/json")
    Evaluation save(@RequestBody Evaluation evaluation) {	
		  System.out.println("*******save ***********");
		  Evaluation s=evaluationservice.saveEvaluation(evaluation);
//		  System.out.println("*******"+s.getEmail());
        return s ;
    }
	
	
	@GetMapping("/{id}")
    Evaluation getEvaluationById(@PathVariable Long id) {
        return evaluationservice.getEvaluation(id);
    }
	
	@GetMapping("/quantity")
    int getQuantityEvaluation() {
        return evaluationservice.getQuantityOfEvaluation();
    }
	
//	@GetMapping("/rendezvousByName/{name}")
//    Rendezvous getRendezvousByName(@PathVariable String name) {
//        return rendezvousservice.findRendezvousByName(name);
//    }
	
	@DeleteMapping("/delete/{id}")
    boolean delete(@PathVariable Long id) {
        evaluationservice.deleteEvaluation(id);
        return true;
    }
	
	
	
}
