package com.isims.education.service.controller;


import com.isims.education.persistance.entities.Conge;
import com.isims.education.service.interfaces.IConge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/conge")
public class CongeController {
    @Autowired
    IConge congeservice;

    @RequestMapping(method = RequestMethod.POST, consumes="application/json", produces = "application/json")
    Conge save(@RequestBody Conge conge) {
        System.out.println("*******save ***********");
        Conge cng = congeservice.saveConge(conge);
        return cng;
    }

    @GetMapping("/{id}")
    Conge getCongeById(@PathVariable Long id) {
        return congeservice.getConge(id);
    }

    @GetMapping("/quantity")
    int getQuantityConge() {
        return congeservice.getQuantityOfConge();
    }



    @DeleteMapping("/delete/{id}")
    boolean delete(@PathVariable Long id) {
        congeservice.deleteConge(id);
        return true;
    }

}
