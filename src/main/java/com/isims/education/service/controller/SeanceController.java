package com.isims.education.service.controller;

import com.isims.education.persistance.entities.Seance;
import com.isims.education.service.interfaces.ISeance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/seance")
public class SeanceController {

	@Autowired
	ISeance seanceservice;
	
	@RequestMapping(method = RequestMethod.POST, consumes="application/json", produces = "application/json")
    Seance save(@RequestBody Seance seance) {	
		  System.out.println("*******save ***********");
		  Seance s=seanceservice.saveSeance(seance);
//		  System.out.println("*******"+s.getEmail());
        return s ;
    }
	
	
	@GetMapping("/{id}")
    Seance getSeanceById(@PathVariable Long id) {
        return seanceservice.getSeance(id);
    }
	
	@GetMapping("/quantity")
    int getQuantitySeance() {
        return seanceservice.getQuantityOfSeance();
    }
	
//	@GetMapping("/rendezvousByName/{name}")
//    Rendezvous getRendezvousByName(@PathVariable String name) {
//        return rendezvousservice.findRendezvousByName(name);
//    }
	
	@DeleteMapping("/delete/{id}")
    boolean delete(@PathVariable Long id) {
        seanceservice.deleteSeance(id);
        return true;
    }
	
	
	
}
