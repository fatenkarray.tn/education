package com.isims.education.service.controller;

import com.isims.education.persistance.entities.Emploi;
import com.isims.education.service.interfaces.IEmploi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/emploi")
public class EmploiController {

	@Autowired
    IEmploi emploiservice;
	
	@RequestMapping(method = RequestMethod.POST, consumes="application/json", produces = "application/json")
    Emploi save(@RequestBody Emploi emploi) {
		  System.out.println("*******save ***********");
		  Emploi conslt=emploiservice.saveEmploi(emploi);
        return conslt ;
    }
	
	
	@GetMapping("/{id}")
    Emploi getEmploiById(@PathVariable Long id) {
        return emploiservice.getEmploi(id);
    }
	
	@GetMapping("/quantity")
    int getQuantityEmploi() {
        return emploiservice.getQuantityOfEmploi();
    }

	
	@DeleteMapping("/delete/{id}")
    boolean delete(@PathVariable Long id) {
        emploiservice.deleteEmploi(id);
        return true;
    }
	
	
	
}
