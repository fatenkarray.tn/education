package com.isims.education.service.controller;

import com.isims.education.service.interfaces.IEnseignant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.isims.education.persistance.entities.Enseignant;

@RestController
@RequestMapping("/api/enseignant")
public class EnseignantController {

	@Autowired
    IEnseignant enseignantservice;
	
	@RequestMapping(method = RequestMethod.POST, consumes="application/json", produces = "application/json")
	Enseignant save(@RequestBody Enseignant enseignant) {	
		  System.out.println("*******save ***********");
		  Enseignant e=enseignantservice.saveEnseignant(enseignant);
		  System.out.println("*******"+e.getEmail());
        return e ;
    }
	
	
	@GetMapping("/{id}")
	Enseignant getEnseignantById(@PathVariable Long id) {
        return enseignantservice.getEnseignant(id);
    }
	
	@GetMapping("/quantity")
    int getQuantityEnseignant() {
        return enseignantservice.getQuantityOfEnseignant();
    }
	
	@GetMapping("/enseignantByName/{name}")
	Enseignant getEnseignantByName(@PathVariable String name) {
        return enseignantservice.findEnseignantByName(name);
    }
	
	@DeleteMapping("/delete/{id}")
    boolean delete(@PathVariable Long id) {
		enseignantservice.deleteEnseignant(id);
        return true;
    }
	
	
	
}
