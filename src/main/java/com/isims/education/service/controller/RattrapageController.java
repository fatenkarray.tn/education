package com.isims.education.service.controller;


import com.isims.education.persistance.entities.Rattrapage;
import com.isims.education.service.interfaces.IRattrapage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/rattrapage")
public class RattrapageController {
    @Autowired
    IRattrapage rattrapageservice;

    @RequestMapping(method = RequestMethod.POST, consumes="application/json", produces = "application/json")
    Rattrapage save(@RequestBody Rattrapage rattrapage) {
        System.out.println("*******save ***********");
        Rattrapage med = rattrapageservice.saveRattrapage(rattrapage);
        System.out.println("*******" + med.getDate());
        return med;
    }

    @GetMapping("/{id}")
    Rattrapage getRattrapageById(@PathVariable Long id) {
        return rattrapageservice.getRattrapage(id);
    }

    @GetMapping("/quantity")
    int getQuantityRattrapage() {
        return rattrapageservice.getQuantityOfRattrapage();
    }

    @GetMapping("/rattrapageBydate/{date}")
    Rattrapage getRattrapageByName(@PathVariable LocalDateTime date) {
        return rattrapageservice.findRattrapageByDate(date);
    }

    @DeleteMapping("/delete/{id}")
    boolean delete(@PathVariable Long id) {
        rattrapageservice.deleteRattrapage(id);
        return true;
    }

}
