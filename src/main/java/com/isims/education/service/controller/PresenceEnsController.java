package com.isims.education.service.controller;

import com.isims.education.persistance.entities.PresenceEns;
import com.isims.education.service.interfaces.IPresenceEns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/presenceEns")
public class PresenceEnsController {

	@Autowired
    IPresenceEns presenceEnsservice;
	
	@RequestMapping(method = RequestMethod.POST, consumes="application/json", produces = "application/json")
    PresenceEns save(@RequestBody PresenceEns presenceEns) {
		  System.out.println("*******save ***********");
		  PresenceEns spec=presenceEnsservice.savePresenceEns(presenceEns);
		  System.out.println("*******"+spec.getDate());
        return spec ;
    }
	
	
	@GetMapping("/{id}")
    PresenceEns getPresenceEnsById(@PathVariable Long id) {
        return presenceEnsservice.getPresenceEns(id);
    }
	

	
	@GetMapping("/presenceEnsByDate/{date}")
    PresenceEns getPresenceEnsByDate(@PathVariable LocalDateTime date) {
        return presenceEnsservice.findPresenceEnsByDate(String.valueOf(date));
    }
	
	@DeleteMapping("/delete/{id}")
    boolean delete(@PathVariable Long id) {
        presenceEnsservice.deletePresenceEns(id);
        return true;
    }
	
	
	
}
