package com.isims.education.service.impliments;

import com.isims.education.persistance.dao.CongeRepository;
import com.isims.education.persistance.entities.Conge;
import com.isims.education.service.interfaces.IConge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CongeService implements IConge {
    @Autowired
    CongeRepository congerepository;

    @Override
    public Conge saveConge(Conge conge) {
        // TODO Auto-generated method stub
        return congerepository.save(conge);
    }

    @Override
    public Conge updateConge(Conge conge){
        // TODO Auto-generated method stub
        return congerepository.saveAndFlush(conge);
    }

    @Override
    public boolean deleteConge(Long id) {
        // TODO Auto-generated method stub
        congerepository.deleteById(id);
        return true;
    }

    @Override
    public List<Conge> getListConge() {
        // TODO Auto-generated method stub
        return  congerepository.findAll();
    }

    @Override
    public Conge getConge(Long id) {
        // TODO Auto-generated method stub
        return  congerepository.findById(id).get();
    }


    @Override
    public int getQuantityOfConge() {
        // TODO Auto-generated method stub
        return  congerepository.getQuantityOfConge();
    }

    @Override
    public Conge getCongeByIdConge(Long id) {
        return congerepository.getCongeByIdConge(id);
    }


}
