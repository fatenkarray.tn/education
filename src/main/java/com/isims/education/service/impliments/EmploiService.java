package com.isims.education.service.impliments;

import com.isims.education.persistance.dao.EmploiRepository;
import com.isims.education.persistance.entities.Emploi;
import com.isims.education.service.interfaces.IEmploi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmploiService implements IEmploi {

	@Autowired
    EmploiRepository emploirepository;
	
	@Override
	public Emploi saveEmploi(Emploi emploi) {
		// TODO Auto-generated method stub
		return emploirepository.save(emploi);
	}

	@Override
	public Emploi updateEmploi(Emploi emploi) {
		// TODO Auto-generated method stub
		return emploirepository.saveAndFlush(emploi);
	}

	@Override
	public boolean deleteEmploi(Long id) {
		// TODO Auto-generated method stub
		 emploirepository.deleteById(id);
		return true;
	}

	@Override
	public List<Emploi> getListEmploi() {
		// TODO Auto-generated method stub
		return  emploirepository.findAll();
	}

	@Override
	public Emploi getEmploi(Long id) {
		// TODO Auto-generated method stub
		return  emploirepository.findById(id).get();
	}

	@Override
	public int getQuantityOfEmploi() {
		// TODO Auto-generated method stub
		return  emploirepository.getQuantityOfEmploi();
	}

	@Override
	public Emploi getEmploiByIdEmploi(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
