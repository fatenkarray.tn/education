package com.isims.education.service.impliments;

import java.util.List;

import com.isims.education.persistance.dao.EnseignantRepository;
import com.isims.education.service.interfaces.IEnseignant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isims.education.persistance.entities.Enseignant;

@Service
public class EnseignantService implements IEnseignant {

	@Autowired
	EnseignantRepository enseignantrepository;
	
	@Override
	public Enseignant saveEnseignant(Enseignant enseignant) {
		// TODO Auto-generated method stub
		return enseignantrepository.save(enseignant);
	}

	@Override
	public Enseignant updateEnseignant(Enseignant enseignant) {
		// TODO Auto-generated method stub
		return enseignantrepository.saveAndFlush(enseignant);
	}

	@Override
	public boolean deleteEnseignant(Long id) {
		// TODO Auto-generated method stub
		enseignantrepository.deleteById(id);
		return true;
	}

	@Override
	public List<Enseignant> getListEnseignant() {
		// TODO Auto-generated method stub
		return  enseignantrepository.findAll();
	}

	@Override
	public Enseignant getEnseignant(Long id) {
		// TODO Auto-generated method stub
		return  enseignantrepository.findById(id).get();
	}

	@Override
	public Enseignant findEnseignantByName(String name) {
		// TODO Auto-generated method stub
		return  enseignantrepository.findByNom(name);
	}

	@Override
	public int getQuantityOfEnseignant() {
		// TODO Auto-generated method stub
		return  enseignantrepository.getQuantityOfEnseignant();
	}

	@Override
	public Enseignant getEnseignantByIdEnseignant(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
