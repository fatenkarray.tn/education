package com.isims.education.service.impliments;

import com.isims.education.persistance.dao.EvaluationRepository;
import com.isims.education.persistance.entities.Evaluation;
import com.isims.education.service.interfaces.IEvaluation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EvaluationService implements IEvaluation {

	@Autowired
	EvaluationRepository evaluationrepository;
	
	@Override
	public Evaluation saveEvaluation(Evaluation evaluation) {
		// TODO Auto-generated method stub
		return evaluationrepository.save(evaluation);
	}

	@Override
	public Evaluation updateEvaluation(Evaluation evaluation) {
		// TODO Auto-generated method stub
		return evaluationrepository.saveAndFlush(evaluation);
	}

	@Override
	public boolean deleteEvaluation(Long id) {
		// TODO Auto-generated method stub
		 evaluationrepository.deleteById(id);
		return true;
	}

	@Override
	public List<Evaluation> getListEvaluation() {
		// TODO Auto-generated method stub
		return  evaluationrepository.findAll();
	}

	@Override
	public Evaluation getEvaluation(Long id) {
		// TODO Auto-generated method stub
		return  evaluationrepository.findById(id).get();
	}

//	@Override
//	public Rendezvous findRendezvousByame(String name) {
//		// TODO Auto-generated method stub
//		return  rendezvousrepository.findByNom(name);
//	}

	@Override
	public int getQuantityOfEvaluation() {
		// TODO Auto-generated method stub
		return evaluationrepository.getQuantityOfEvaluation();
	}

	@Override
	public Evaluation getEvaluationByIdEvaluation(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
