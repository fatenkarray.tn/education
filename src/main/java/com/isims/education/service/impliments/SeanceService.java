package com.isims.education.service.impliments;

import com.isims.education.persistance.dao.SeanceRepository;
import com.isims.education.persistance.entities.Seance;
import com.isims.education.service.interfaces.ISeance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeanceService implements ISeance {

	@Autowired
	SeanceRepository seancerepository;
	
	@Override
	public Seance saveSeance(Seance seance) {
		// TODO Auto-generated method stub
		return seancerepository.save(seance);
	}

	@Override
	public Seance updateSeance(Seance seance) {
		// TODO Auto-generated method stub
		return seancerepository.saveAndFlush(seance);
	}

	@Override
	public boolean deleteSeance(Long id) {
		// TODO Auto-generated method stub
		 seancerepository.deleteById(id);
		return true;
	}

	@Override
	public List<Seance> getListSeance() {
		// TODO Auto-generated method stub
		return  seancerepository.findAll();
	}

	@Override
	public Seance getSeance(Long id) {
		// TODO Auto-generated method stub
		return  seancerepository.findById(id).get();
	}

//	@Override
//	public Rendezvous findRendezvousByame(String name) {
//		// TODO Auto-generated method stub
//		return  rendezvousrepository.findByNom(name);
//	}

	@Override
	public int getQuantityOfSeance() {
		// TODO Auto-generated method stub
		return seancerepository.getQuantityOfSeance();
	}

	@Override
	public Seance getSeanceByIdSeance(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
