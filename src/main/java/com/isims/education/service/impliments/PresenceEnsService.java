package com.isims.education.service.impliments;

import com.isims.education.persistance.dao.PresenceEnsRepository;
import com.isims.education.persistance.entities.PresenceEns;
import com.isims.education.service.interfaces.IPresenceEns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PresenceEnsService implements IPresenceEns {

	@Autowired
	PresenceEnsRepository presenceEnsrepository;
	
	@Override
	public PresenceEns savePresenceEns(PresenceEns presenceEns) {
		// TODO Auto-generated method stub
		return presenceEnsrepository.save(presenceEns);
	}

	@Override
	public PresenceEns updatePresenceEns(PresenceEns presenceEns) {
		// TODO Auto-generated method stub
		return presenceEnsrepository.saveAndFlush(presenceEns);
	}

	@Override
	public boolean deletePresenceEns(Long id) {
		// TODO Auto-generated method stub
		 presenceEnsrepository.deleteById(id);
		return true;
	}

	@Override
	public List<PresenceEns> getListPresenceEns() {
		// TODO Auto-generated method stub
		return  presenceEnsrepository.findAll();
	}

	@Override
	public PresenceEns getPresenceEns(Long id) {
		// TODO Auto-generated method stub
		return  presenceEnsrepository.findById(id).get();
	}

	@Override
	public PresenceEns findPresenceEnsByDate(String date) {
		// TODO Auto-generated method stub
		return  presenceEnsrepository.findByDate(date);
	}


	@Override
	public PresenceEns getPresenceEnsByIdPresenceEns(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
