package com.isims.education.service.impliments;

import com.isims.education.persistance.dao.RattrapageRepository;
import com.isims.education.persistance.entities.Rattrapage;
import com.isims.education.service.interfaces.IRattrapage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RattrapageService implements IRattrapage {
    @Autowired
    RattrapageRepository rattrapagerepository;

    @Override
    public Rattrapage saveRattrapage(Rattrapage rattrapage) {
        // TODO Auto-generated method stub
        return rattrapagerepository.save(rattrapage);
    }

    @Override
    public Rattrapage updateRattrapage(Rattrapage rattrapage){
        // TODO Auto-generated method stub
        return rattrapagerepository.saveAndFlush(rattrapage);
    }

    @Override
    public boolean deleteRattrapage(Long id) {
        // TODO Auto-generated method stub
        rattrapagerepository.deleteById(id);
        return true;
    }

    @Override
    public List<Rattrapage> getListRattrapage() {
        // TODO Auto-generated method stub
        return  rattrapagerepository.findAll();
    }

    @Override
    public Rattrapage getRattrapage(Long id) {
        // TODO Auto-generated method stub
        return  rattrapagerepository.findById(id).get();
    }

    @Override
    public Rattrapage findRattrapageByDate(LocalDateTime date) {
        // TODO Auto-generated method stub
        return  rattrapagerepository.findByDate(String.valueOf(date));
    }

    @Override
    public int getQuantityOfRattrapage() {
        // TODO Auto-generated method stub
        return  rattrapagerepository.getQuantityOfRattrapage();
    }

    @Override
    public Rattrapage getRattrapageByIdRattrapage(Long id) {
        return rattrapagerepository.getRattrapageByIdRattrapage(id);
    }


}
