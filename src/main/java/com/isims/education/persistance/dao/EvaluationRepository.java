package com.isims.education.persistance.dao;

import com.isims.education.persistance.entities.Evaluation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EvaluationRepository extends JpaRepository<Evaluation,Long> {
	


    @Query(value = "select count(*) from evaluation",nativeQuery = true)
    int getQuantityOfEvaluation();
    @Query(value = "select * from evaluation where id= :id",nativeQuery = true)
    Evaluation getEvaluationByIdEvaluation(@Param("id") Long id);


}
