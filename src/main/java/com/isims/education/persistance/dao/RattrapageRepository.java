package com.isims.education.persistance.dao;

import com.isims.education.persistance.entities.Rattrapage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RattrapageRepository extends JpaRepository<Rattrapage,Long> {
	
	Rattrapage findByDate(String date);
	

    @Query(value = "select count(*) from rattrapage",nativeQuery = true)
    int getQuantityOfRattrapage();
    @Query(value = "select * from rattrapage where id= :id",nativeQuery = true)
    Rattrapage getRattrapageByIdRattrapage(@Param("id") Long id);

}
