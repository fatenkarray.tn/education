package com.isims.education.persistance.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.isims.education.persistance.entities.Enseignant;
public interface EnseignantRepository extends JpaRepository<Enseignant,Long> {
	
	Enseignant findByNom(String nom);
	

    @Query(value = "select count(*) from enseignant",nativeQuery = true)
    int getQuantityOfEnseignant();
    @Query(value = "select * from enseignant where id= :id",nativeQuery = true)
    Enseignant getPatientByIdEnseignant(@Param("id") Long id);

}
