package com.isims.education.persistance.dao;

import com.isims.education.persistance.entities.PresenceEns;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PresenceEnsRepository extends JpaRepository<PresenceEns,Long> {
	
	PresenceEns findByDate(String date);


}
