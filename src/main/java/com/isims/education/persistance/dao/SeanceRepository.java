package com.isims.education.persistance.dao;

import com.isims.education.persistance.entities.Seance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SeanceRepository extends JpaRepository<Seance,Long> {
	


    @Query(value = "select count(*) from seance",nativeQuery = true)
    int getQuantityOfSeance();
    @Query(value = "select * from seance where id= :id",nativeQuery = true)
    Seance getSeanceByIdSeance(@Param("id") Long id);


}
