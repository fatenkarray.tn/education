package com.isims.education.persistance.dao;

import com.isims.education.persistance.entities.Conge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CongeRepository extends JpaRepository<Conge,Long> {



    @Query(value = "select count(*) from conge",nativeQuery = true)
    int getQuantityOfConge();
    @Query(value = "select * from conge where id= :id",nativeQuery = true)
    Conge getCongeByIdConge(@Param("id") Long id);

}
