package com.isims.education.persistance.dao;

import com.isims.education.persistance.entities.Emploi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EmploiRepository extends JpaRepository<Emploi,Long> {
	
//	Emploi findByNom(String nom);
	

    @Query(value = "select count(*) from emploi",nativeQuery = true)
    int getQuantityOfEmploi();
    @Query(value = "select * from emploi where id= :id",nativeQuery = true)
    Emploi getEmploiByIdEmploi(@Param("id") Long id);

}
