package com.isims.education.persistance.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class PresenceEns implements Serializable {
	public PresenceEns(){super();}

	public PresenceEns(Long id, LocalDateTime date, String etat, List<Seance> seances) {
		this.id = id;
		this.date = date;
		this.etat = etat;
		this.seances = seances;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	 public List<Seance> getSeances() {
	 		return seances;
	 	}

	 	public void setSeances(List<Seance> seances) {
	 		this.seances = seances;
	 	}

	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
		private LocalDateTime date;
	    private String etat;
	@JsonIgnore
   @OneToMany (fetch = FetchType.LAZY,mappedBy = "presenceEns")
	private List<Seance> seances;

	

}
