package com.isims.education.persistance.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import javax.persistence.*;

@Entity
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
public class Emploi implements Serializable {
	public Emploi(){super();}

	public Emploi(Long id, String jour, String groupe, String matiere, String h_debut, String h_fin, Enseignant enseignant) {
		this.id = id;
		this.jour = jour;
		this.groupe = groupe;
		this.matiere = matiere;
		this.h_debut = h_debut;
		this.h_fin = h_fin;
		this.enseignant = enseignant;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJour() {
		return jour;
	}

	public void setJour(String jour) {
		this.jour = jour;
	}

	public String getGroupe() {
		return groupe;
	}

	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	public String getH_debut() {
		return h_debut;
	}

	public void setH_debut(String h_debut) {
		this.h_debut = h_debut;
	}

	public String getH_fin() {
		return h_fin;
	}

	public void setH_fin(String h_fin) {
		this.h_fin = h_fin;
	}

	public Enseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
		private String jour;
		private String groupe;
		private String matiere;
		private String h_debut;
		private String h_fin;
		@OneToOne (fetch = FetchType.EAGER)
	    private Enseignant enseignant;

	

}
