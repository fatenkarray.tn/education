package com.isims.education.persistance.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
//@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Seance implements Serializable {
    public Seance(){super();}
    public Seance(Long id, LocalDateTime dateS, String heureS,String heureF,String salle,String matiere, Enseignant enseignant, PresenceEns presenceEns) {
        this.id = id;
        this.dateS = dateS;
        this.heureS = heureS;
        this.heureF = heureF;
        this.salle = salle;
        this.matiere = matiere;
        this.enseignant = enseignant;
        this.presenceEns = presenceEns;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateS() {
        return dateS;
    }

    public void setDateS(LocalDateTime dateS) {
        this.dateS = dateS;
    }

    public String getHeureS() {
        return heureS;
    }

    public void setHeureS(String heureS) {
        this.heureS = heureS;
    }
    
    public String getHeureF() {
        return heureF;
    }

    public void setHeureF(String heureF) {
        this.heureF = heureF;
    }
    
    public String getSalle() {
        return salle;
    }

    public void setSalle(String salle) {
        this.salle = salle;
    }
    
    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    public PresenceEns getPresenceEns() {
        return presenceEns;
    }

    public void setPresenceEns(PresenceEns presenceEns) {
        this.presenceEns = presenceEns;
    }

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime dateS;
    private String heureS;
    private String heureF;
    private String salle;
    private String matiere;
    @ManyToOne
    private Enseignant enseignant;
    @ManyToOne
    private PresenceEns presenceEns;
    
}