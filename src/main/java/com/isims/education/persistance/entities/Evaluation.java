package com.isims.education.persistance.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
//@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Evaluation implements Serializable {
    public Evaluation(){super();}

    public Evaluation(Long id, String matiere, Float note_tp, Float note_ds, Float note_examen, Float moyenne, Enseignant enseignant) {
        this.id = id;
        this.matiere = matiere;
        this.note_tp = note_tp;
        this.note_ds = note_ds;
        this.note_examen = note_examen;
        this.moyenne = moyenne;
        this.enseignant = enseignant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public Float getNote_tp() {
        return note_tp;
    }

    public void setNote_tp(Float note_tp) {
        this.note_tp = note_tp;
    }

    public Float getNote_ds() {
        return note_ds;
    }

    public void setNote_ds(Float note_ds) {
        this.note_ds = note_ds;
    }

    public Float getNote_examen() {
        return note_examen;
    }

    public void setNote_examen(Float note_examen) {
        this.note_examen = note_examen;
    }

    public Float getMoyenne() {
        return moyenne;
    }

    public void setMoyenne(Float moyenne) {
        this.moyenne = moyenne;
    }

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String matiere;
    private Float note_tp;
    private Float note_ds;
    private Float note_examen;
    private Float moyenne;
    @ManyToOne
    private Enseignant enseignant;
//    @ManyToOne
//    private Etudiant etudiant;
    
}