package com.isims.education.persistance.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Enseignant implements Serializable{

	  public Enseignant() {
		super();
	}

	public Enseignant(Long id, String nom, String prenom, String adr, String email, String telephone, String departement, List<Seance> seanceList, List<Evaluation> evaluationList, List<Conge> congeList, List<Rattrapage> rattrapageList) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.adr = adr;
		this.email = email;
		this.telephone = telephone;
		this.departement = departement;
		this.seanceList = seanceList;
		this.evaluationList = evaluationList;
		this.congeList = congeList;
		this.rattrapageList = rattrapageList;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdr() {
		return adr;
	}
	public void setAdr(String adr) {
		this.adr = adr;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getDepartement() {
		return departement;
	}
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	public List<Seance> getSeanceList() {
		return seanceList;
	}
	public void setSeanceList(List<Seance> seanceList) {
		this.seanceList = seanceList;
	}
	
	public List<Conge> getCongeList() {
		return congeList;
	}
	public void setCongeList(List<Conge> congeList) {
		this.congeList = congeList;
	}
	
	public List<Rattrapage> getRattrapageList() {
		return rattrapageList;
	}
	public void setRattrapageList(List<Rattrapage> rattrapageList) {
		this.rattrapageList = rattrapageList;
	}

	public List<Evaluation> getEvaluationList() {
		return evaluationList;
	}

	public void setEvaluationList(List<Evaluation> evaluationList) {
		this.evaluationList = evaluationList;
	}

	@Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  private Long id;
	  private String nom;
	  private String prenom;
	  private String adr;
	  private String email;
	  private String telephone;
	  private String departement;
	  
	  @JsonIgnore
	  @OneToMany(mappedBy="enseignant",fetch=FetchType.LAZY)
	  private List<Seance>seanceList;
	  @OneToMany(mappedBy="enseignant",fetch=FetchType.LAZY)
	  private List<Evaluation>evaluationList;
	  @OneToMany(mappedBy="enseignant",fetch=FetchType.LAZY)
	  private List<Conge>congeList;
	  @OneToMany(mappedBy="enseignant",fetch=FetchType.LAZY)
	  private List<Rattrapage>rattrapageList;
	
}
