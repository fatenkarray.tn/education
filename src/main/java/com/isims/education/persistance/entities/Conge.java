package com.isims.education.persistance.entities;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
//@NoArgsConstructor @AllArgsConstructor
public class Conge implements Serializable {
    public Conge() {
        super();
    }

    public Conge(Long id, LocalDateTime date_debut, LocalDateTime date_fin, String duree, String raison, Enseignant enseignant) {
        this.id = id;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.duree = duree;
        this.raison = raison;
        this.enseignant = enseignant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(LocalDateTime date_debut) {
        this.date_debut = date_debut;
    }

    public LocalDateTime getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(LocalDateTime date_fin) {
        this.date_fin = date_fin;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getRaison() {
        return raison;
    }

    public void setRaison(String raison) {
        this.raison = raison;
    }
    public Enseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime date_debut;
    private LocalDateTime date_fin;
    private String duree;
    private String raison;
    
    @ManyToOne
    private Enseignant enseignant;




}