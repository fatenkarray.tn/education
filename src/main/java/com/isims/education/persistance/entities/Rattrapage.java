package com.isims.education.persistance.entities;



import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
//@NoArgsConstructor @AllArgsConstructor
public class Rattrapage implements Serializable {
    public Rattrapage() {
        super();
    }

    public Rattrapage(Long id, LocalDateTime date, String duree, String heure, String matiere, Enseignant enseignant) {
        this.id = id;
        this.date = date;
        this.duree = duree;
        this.heure = heure;
        this.matiere = matiere;
        this.enseignant = enseignant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }
    
    public Enseignant getEnseignant() {
 		return enseignant;
 	}

 	public void setEnseignant(Enseignant enseignant) {
 		this.enseignant = enseignant;
 	}


    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime date;
    private String duree;
    private String heure;
    private String matiere;

    @ManyToOne
    private Enseignant enseignant;


}